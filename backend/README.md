# Case Join Tasks - API Backend

## Instruções para Configuração do Projeto

### 1. Configuração do Arquivo `.env`
Após clonar o repositório, copie o arquivo `.env.example` na raiz do projeto com o nome `.env`:

```
cp .env.example .env
```

### 2. Subir os Containers
Criar uma rede para o host:
```
docker network create backend
```
Para subir os containers nginx, php, e postgres, execute o comando:

```
docker-compose up -d --build
```

### 3. Entrar no Container PHP
Para acessar o container PHP, use o comando:

```
docker-compose exec api_app bash
```

### 4. Instalar as Dependências e Configurar o Projeto
Dentro do container PHP, execute os seguintes comandos:

- Instalar as dependências do projeto:
```
composer install
```
- Definir a chave APP_KEY no .env:
```
php artisan key:generate
```
- Definir a chave JWT_SECRET no .env:
```
php artisan jwt:secret
```
- Gerar as tabelas do banco via migrations:

```
php artisan migrate
```
- Persistir dados de usuário na tabela users via seeders:

```
php artisan db:seed
```

### 5. Testes unitários
Dentro do container PHP, execute os seguinte comando:

- Rodar testes unitários de autenticação de usuários:
```
php artisan test
```

#### Para sair do container: `exit`.

### Documentação da API com Scribe: `http://localhost/docs/index.html`

