<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use Tests\TestCase;

/**
 *
 * @title User Module
 *
 * @testdox User Auth Module
 *
 * @description Manage all the user auth.
 *
 **/
class LoginControllerTest extends TestCase
{
    private string $endpoint = '/api/auth';

    /**
     *
     * @testdox Login User
     *
     * @description Login user via POST request.
     *
     **/
    public function testLogin()
    {
        $password = 'secret';
        $user = factory(User::class)->create([
            'password' => $password
        ]);

        $response = $this
            ->postJson($this->endpoint . '/login', [
                'email' => $user->email,
                'password' => $password,
            ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    public function testLoginInvalid()
    {
        $password = 'password';
        $user = factory(User::class)->create([
            'password' => $password
        ]);

        $response = $this->postJson($this->endpoint . '/login', [
            'email' => $user->email,
            'password' => 'incorrect_password'
        ]);

        $response->assertStatus(401);
        $response->assertJsonPath('error', 'Credenciais inválidas!');
    }

    public function testReturnDataLogged()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user, 'users_api')
            ->getJson($this->endpoint . '/me');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'created_at' => $user->created_at->toJSON(),
                'updated_at' => $user->updated_at->toJSON(),
            ]
        ]);
    }

    public function testNotAuthenticated()
    {
        $response = $this->getJson($this->endpoint . '/me');
        $response->assertStatus(401);
        $response->assertJsonPath('message', 'Unauthenticated.');
    }

    /**
     *
     * @testdox Logout User
     *
     * @description Logout user via POST request.
     *
     **/
    public function testLogout()
    {
        $user = factory(User::class)->create();
        $token = auth('users_api')->login($user);

        $response = $this->actingAs($user, 'users_api')
            ->withHeader('Authorization', 'Bearer ' . $token)
            ->postJson($this->endpoint . '/logout');

        $response->assertStatus(200);
        $response->assertJson([]);
    }

    public function testInvalidToken()
    {
        $response = $this->withHeader('Authorization', 'Bearer ashauhaushua')
            ->getJson($this->endpoint . '/me');

        $response->assertStatus(401);
        $response->assertJsonPath('message', 'Unauthenticated.');
    }

    /**
     *
     * @testdox Refresh Token User
     *
     * @description Refresh Token User via POST request.
     *
     **/
    public function testRefreshToken()
    {
        $user = factory(User::class)->create();
        $token = auth('users_api')->login($user);

        $response = $this->actingAs($user, 'users_api')
            ->withHeader('Authorization', 'Bearer ' . $token)
            ->postJson($this->endpoint . '/refresh');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    public function testLoginWithEmptyData()
    {
        $response = $this->postJson($this->endpoint . '/login', []);

        $response
            ->assertStatus(422)
            ->assertJsonStructure([
                'errors' => ['email', 'password'],
            ]);
    }
}
