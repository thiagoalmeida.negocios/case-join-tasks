<?php

use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HealthCheckController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [HealthCheckController::class, 'index'])->name('healthcheck.index');

Route
    ::prefix('/auth')
    ->namespace('Http/Controllers')
    ->group(function () {
        Route::post('/login', [UserController::class, 'login']);
        Route::post('/logout', [UserController::class, 'logout']);
        Route::post('/refresh', [UserController::class, 'refresh']);
        Route::get('/me', [UserController::class, 'me']);
    });

Route::middleware('auth:users_api')->group(function () {
    Route::resource('/tasks', 'TaskController');
    Route::post('/tasks/{id}/assign', [TaskController::class, 'assign']);
});
