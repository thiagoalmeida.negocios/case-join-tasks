<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->createMany(
            [
                ['name' => 'João', 'email' => 'joao@email.com', 'password' => '123456'],
                ['name' => 'Maria', 'email' => 'maria@email.com', 'password' => '123456'],
            ]
        );
    }
}
