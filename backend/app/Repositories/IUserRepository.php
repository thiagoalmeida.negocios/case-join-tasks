<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

interface IUserRepository
{
    public function getByEmail(string $email): ?Model;
}
