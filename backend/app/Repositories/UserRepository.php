<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserRepository implements IUserRepository
{

    public function getByEmail(string $email): ?Model
    {
        return User::query()->where('email', $email)->first();
    }
}
