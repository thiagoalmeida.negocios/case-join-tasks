<?php

namespace App\Repositories;

use App\Models\Task\Task;
use App\Models\Task\Filter\TaskFilters;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class TaskRepository implements ITaskRepository
{
    public function __construct(protected Builder $query)
    {
        $this->query = Task::query();
    }

    public function getAll(): Collection
    {
        return $this->query
            ->where('user_id', JWTAuth::user()->id)
            ->orderBy('title')
            ->get([
                'id',
                'title',
                'description'
            ]);
    }

    public function create(array $data): Model
    {
        $task = new Task();
        return $this->edit($task, $data);
    }

    public function getById(string $id): ?Model
    {
        return $this->query->find($id);
    }

    public function update(Model $task, array $data): ?Model
    {
        return $this->edit($task, $data, true);
    }

    private function edit(Model $task, array $data, bool $update = false): Model
    {
        $task->title = $data['title'];
        $task->description = array_key_exists('description', $data) ? $data['description'] : null;
        $task->save();

        if (!$update) {
            $task->users()->attach(JWTAuth::user()->id, ['is_creator' => true]);
        }

        return $task;
    }

    public function delete(Model $task): bool
    {
        return $task->delete();
    }

    public function getAllByFilter(TaskFilters $filters): Collection
    {
        return Task::filter($filters)
            ->join('task_users', 'task_users.task_id', '=', 'tasks.id')
            ->where('task_users.user_id', JWTAuth::user()->id)
            ->orderBy('title')
            ->get([
                'tasks.id',
                'tasks.title',
                'tasks.description',
                'tasks.created_at',
                'tasks.updated_at'
            ]);
    }

    public function assign(Model $task, array $data): void
    {
        foreach ($data['users'] as $user) {
            $task->users()->attach($user['id']);
        }
    }
}
