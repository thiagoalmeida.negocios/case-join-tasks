<?php

namespace App\Repositories;

use App\Models\Task\Filter\TaskFilters;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface ITaskRepository extends IRepository
{
    public function getAllByFilter(TaskFilters $filters): Collection;
    public function assign(Model $task, array $data): void;
}
