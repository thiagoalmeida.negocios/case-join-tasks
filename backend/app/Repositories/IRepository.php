<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

interface IRepository
{
    public function getAll(): Collection;
    public function create(array $data): Model;
    public function getById(string $id): Model|Builder|null;
    public function update(Model $model, array $data): ?Model;
    public function delete(Model $model): bool;
}
