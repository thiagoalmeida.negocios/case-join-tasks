<?php

namespace App\Models;

use App\Models\Entity\AuthenticatableEntity;
use Illuminate\Support\Facades\Hash;

class User extends AuthenticatableEntity
{
    protected $hidden = ['password'];

    public $keyType = 'uuid';

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

}
