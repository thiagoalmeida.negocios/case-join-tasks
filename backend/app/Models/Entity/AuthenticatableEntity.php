<?php

namespace App\Models\Entity;

use Illuminate\Foundation\Auth\User;
use Tymon\JWTAuth\Contracts\JWTSubject;

abstract class AuthenticatableEntity extends User implements JWTSubject
{
    use Uuid;

    public $incrementing = false;
    protected $primaryKey = 'id';

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
