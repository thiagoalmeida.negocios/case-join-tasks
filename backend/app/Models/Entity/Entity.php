<?php

namespace App\Models\Entity;

use Illuminate\Database\Eloquent\Model;

abstract class Entity extends Model
{
    use Uuid;

    public $incrementing = false;
}
