<?php

namespace App\Models\Task;

use App\Models\Entity\Entity;
use App\Models\Task\Filter\TaskFilters;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Task extends Entity
{
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'task_users', 'task_id', 'user_id')
            ->withPivot(['id', 'is_creator'])
            ->withTimestamps()
            ->using(TaskUser::class);
    }

    public function scopeFilter(Builder $query, TaskFilters $filters): Builder
    {
        return $filters->apply($query);
    }
}
