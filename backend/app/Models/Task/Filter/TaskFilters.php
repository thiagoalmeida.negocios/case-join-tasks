<?php

namespace App\Models\Task\Filter;

use Illuminate\Database\Eloquent\Builder;

class TaskFilters
{
    protected array $filters = [
        'title' => TaskTitleFilter::class,
    ];

    public function apply($query): Builder
    {
        foreach ($this->receivedFilters() as $name => $value) {
            $filterInstance = new $this->filters[$name];
            $query = $filterInstance($query, $value);
        }

        return $query;
    }

    public function receivedFilters(): array
    {
        return request()->only(array_keys($this->filters));
    }
}
