<?php

namespace App\Models\Task\Filter;

use Illuminate\Database\Eloquent\Builder;

class TaskTitleFilter
{
    public function __invoke(Builder $query, string $filter): Builder
    {
        return $query->where('title', 'ilike', "%$filter%");
    }
}
