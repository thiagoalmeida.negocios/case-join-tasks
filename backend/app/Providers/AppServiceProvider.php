<?php

namespace App\Providers;

use App\Repositories\{
    UserRepository,
    IUserRepository,
    ITaskRepository,
    TaskRepository
};
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            IUserRepository::class,
            UserRepository::class
        );
        $this->app->bind(
            ITaskRepository::class,
            TaskRepository::class
        );
    }
}
