<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function __construct(protected UserRepository $userRepository) {}

    public function login(string $email, string $password): ?Model
    {
        $user = $this->userRepository->getByEmail($email);

        if (!$user ) {
            return null;
        }

        return Hash::check($password, $user->password) ? $user  : null;
    }
}
