<?php

namespace App\Services;

use App\Models\Task\Filter\TaskFilters;
use App\Repositories\ITaskRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TaskService
{
    public function __construct(protected ITaskRepository $taskRepository) {}

    public function getAllByFilter(TaskFilters $filters): Collection
    {
        return $this->taskRepository->getAllByFilter($filters);
    }

    public function create(array $data): Model
    {
        return $this->taskRepository->create($data);
    }

    public function getById(string $id): ?Model
    {
        return $this->taskRepository->getById($id);
    }

    public function update(string $id, array $data): ?Model
    {
        $task = $this->taskRepository->getById($id);

        if (!$task) {
            return null;
        }

        return $this->taskRepository->update($task, $data);
    }

    public function delete(string $id): bool
    {
        $task = $this->taskRepository->getById($id);

        if (!$task) {
            return false;
        }

        return $this->taskRepository->delete($task);
    }

    public function assign(array $data, string $id): bool
    {
        $task = $this->taskRepository->getById($id);

        if (!$task) {
            return false;
        }

        $this->taskRepository->assign($task, $data);

        return true;
    }

}
