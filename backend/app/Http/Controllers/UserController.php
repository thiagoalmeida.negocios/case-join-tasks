<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\UserService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function __construct(private UserService $userService)
    {
        $this->middleware('auth:users_api', ['except' => ['login']]);
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $user = $this->userService->login($request->get('email'), $request->get('password'));

        if (!$user) {
            return response()->json(['error'=> 'Credenciais inválidas!'], Response::HTTP_UNAUTHORIZED);
        }

        $token = $this->guard()->login($user);

        return $this->respondWithToken($token);
    }

    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('users_api')->factory()->getTTL()
        ]);
    }

    public function guard(): Guard
    {
        return auth('users_api');
    }

    public function me(): JsonResponse
    {
        return response()->json([
            'data' => $this->guard()->user(),
        ]);
    }

    public function logout(): JsonResponse
    {
        $this->guard()->logout();
        return response()->json([]);
    }

    public function refresh(): JsonResponse
    {
        $token = $this->guard()->refresh();
        return $this->respondWithToken($token);
    }

}
