<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskAssignRequest;
use App\Http\Requests\TaskRequest;
use App\Models\Task\Filter\TaskFilters;
use App\Services\TaskService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    public function __construct(protected TaskService $taskService) {}

    public function index(TaskFilters $filters): JsonResponse
    {
        $data = $this->taskService->getAllByFilter($filters);
        return response()->json(['data' => $data]);
    }

    public function store(TaskRequest $request): JsonResponse
    {
        $task = $this->taskService->create($request->validated());
        return response()->json(['data' => $task], Response::HTTP_CREATED);
    }

    public function show(string $id): JsonResponse
    {
        $task = $this->taskService->getById($id);

        if (!$task) {
            return response()->json(['error' => 'Tarefa não encontrada.'], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['data' => $task]);
    }

    public function update(TaskRequest $request, string $id): JsonResponse
    {
        $task = $this->taskService->update($id, $request->validated());

        if (!$task) {
            return response()->json(['error' => 'Tarefa não encontrada.'], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['data' => $task], RESPONSE::HTTP_ACCEPTED);
    }

    public function destroy(string $id): JsonResponse
    {
        $task = $this->taskService->delete($id);

        if (!$task) {
            return response()->json(['error' => 'Tarefa não encontrado.'], Response::HTTP_NOT_FOUND);
        }

        return response()->json([]);
    }

    public function assign(TaskAssignRequest $request, string $id): JsonResponse
    {
        $task = $this->taskService->assign($request->validated(), $id);

        if (!$task) {
            return response()->json(['error' => 'Tarefa não encontrado.'], Response::HTTP_NOT_FOUND);
        }

        return response()->json(['data' => 'Tarefa atribuída com sucesso.']);
    }

}
