const loginForm = document.getElementById('login-form');
const httpClient = new HttpClient();

loginForm.addEventListener('submit', function(event) {
    event.preventDefault();

    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    httpClient.post('/auth/login', { email, password })
        .then(login => {

            if (login.error) {
                alert(login.error);
                return
            }
            localStorage.setItem('token', login.access_token);
            window.location.href = 'index.html';

        })
        .catch(error => console.error('Erro ao fazer login:', error));

});

