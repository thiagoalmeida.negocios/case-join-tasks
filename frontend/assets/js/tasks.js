const taskBtn = document.getElementById('task-btn');
const newTaskInput = document.getElementById('new-task');
const todoList = document.getElementById('todo-list');
const filterRadio = document.getElementById('filter-radio');
const addRadio = document.getElementById('add-radio');
const logoutBtn = document.getElementById('logout-btn');

const httpClient = new HttpClient();

logoutBtn.addEventListener('click', logout);

function logout() {
    httpClient.post('/auth/logout')
        .then(() => {
            localStorage.removeItem('token');
            window.location.href = 'login.html';
        })
        .catch(error => console.error('Erro ao fazer logout:', error));
}

loadTasks();

function loadTasks() {
    httpClient.get('/tasks')
        .then(tasks => {
            todoList.innerHTML = '';
            tasks.data.forEach((task) => {
                createTaskElement(task);
            });
        })
        .catch(error => console.error('Erro ao carregar tarefas:', error));
}

taskBtn.addEventListener('click', handleAddTask);

function handleAddTask(event) {
    event.preventDefault();
    const taskTitle = newTaskInput.value.trim();
    if (addRadio.checked) {
        addTask(taskTitle);
    } else if (filterRadio.checked) {
        filterTasks(taskTitle);
    }
}

const addTask = () => {
    const taskTitle = newTaskInput.value.trim();
    if (taskTitle === '') {
        alert('O título da tarefa é obrigatório.');
        return;
    }
    httpClient.post('/tasks', { title: taskTitle })
        .then(task => {
            createTaskElement(task.data);
            newTaskInput.value = '';
            newTaskInput.focus();
        })
        .catch(error => console.error('Erro ao adicionar tarefa:', error));
}

const filterTasks = (taskTitle) => {
    const filter = taskTitle !== '' ? `?title=${taskTitle}` : '';
    httpClient.get(`/tasks${filter}`)
        .then(tasks => {
            todoList.innerHTML = '';
            tasks.data.forEach(task => {
                createTaskElement(task);
            });
        })
        .catch(error => console.error('Erro ao filtrar tarefas:', error));
}

const editTask = (id, taskSpan) => {
    const newTaskTitle = prompt('Editar a tarefa:', taskSpan.textContent);
    if (newTaskTitle !== null && newTaskTitle.trim() !== '') {
        httpClient.put(`/tasks/${id}`, { title: newTaskTitle.trim() })
            .then(updatedTask => {
                taskSpan.textContent = updatedTask.data.title;
            })
            .catch(error => console.error('Erro ao editar tarefa:', error));
    }
}

const deleteTask = (id, taskElement) => {
    httpClient.delete(`/tasks/${id}`)
        .then(() => {
            taskElement.remove();
        })
        .catch(error => console.error('Erro ao excluir tarefa:', error));
}

const assignTask = (id) => {
    alert('Em breve...');
    // TODO atribuir tarefa a outros usuários
}

const createTaskElement = (task) => {
    const li = document.createElement('li');
    const taskSpan = createSpan(task.title);
    li.appendChild(taskSpan);

    const assignBtn = createButton('Atribuir', 'assign-task-btn', () => assignTask(task.id));
    const editBtn = createButton('Editar', 'edit-btn', () => editTask(task.id, taskSpan));
    const deleteBtn = createButton('Excluir', 'delete-btn', () => deleteTask(task.id, li));

    li.appendChild(assignBtn);
    li.appendChild(editBtn);
    li.appendChild(deleteBtn);

    todoList.appendChild(li);
}

const createSpan = (title) => {
    const span = document.createElement('span');
    span.textContent = title;
    return span;
}

const createButton = (text, className, clickHandler) => {
    const btn = document.createElement('button');
    btn.textContent = text;
    btn.classList.add(className);
    btn.addEventListener('click', clickHandler);
    return btn;
}

filterRadio.addEventListener('change', function() {
    taskBtn.textContent = 'Filtrar';
    newTaskInput.placeholder = 'Filtrar por título da tarefa';
});

addRadio.addEventListener('change', function() {
    taskBtn.textContent = 'Adicionar';
    newTaskInput.placeholder = 'Adicionar nova tarefa';
});


