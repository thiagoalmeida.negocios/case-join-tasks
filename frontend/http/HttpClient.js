class HttpClient {

    constructor() {
        this.baseURL = 'http://127.0.0.1/api';
    }

    async get(url) {
        return this.request(url, 'GET');
    }

    async post(url, params = {}) {
        return this.request(url, 'POST', params);
    }

    async put(url, params = {}) {
        return this.request(url, 'PUT', params);
    }

    async delete(url) {
        return this.request(url, 'DELETE');
    }

    async request(url, method, params) {
        const headers = this.getHeaders();
        const options = {
            method,
            headers
        };
        if (params) {
            options.body = JSON.stringify(params);
        }

        try {
            const response = await fetch(`${this.baseURL}${url}`, options);
            if (response.status === 401 && url !== '/auth/login') {
                window.location.href = 'login.html';
                return;
            }
            return response.json();
        } catch (error) {
            console.error('Erro na requisição:', error);
            throw error;
        }
    }

    getHeaders() {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };

        const token = localStorage.getItem('token');

        if (token) {
            headers['Authorization'] = `Bearer ${token}`;
        }

        return headers;
    }
}
