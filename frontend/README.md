# Case Join Tasks - Frontend

## Instruções para inicializar o Projeto

### 1. Configurar API Backend
Após clonar o repositório, realizar as configurações descritas no arquivo README.md, que se encontra dentro da pasta `./backend`:

### 2. Acessar o sistema
Para acessar o sistema, abrir o arquivo que se encontra no caminho `./frontend/src/login.html` no browser de sua preferência.

### Para logar, utilize um dos usuários e senha abaixo:
```
E-mail: joao@email.com ou maria@email.com
Senha: 123456
```

### Observação importante: 
```
Caso queria mudar a porta, por exemplo, para qualquer outra em APP_PORT 
contida em ./backend/.env, deverá ser alterado também em 
./frontend/http/HttpClient.js por exemplo, de 'http://127.0.0.1/api' 
para 'http://127.0.0.1:<nova_porta>/api' na linha 4; 
```