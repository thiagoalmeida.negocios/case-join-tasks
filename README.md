# Case Join Tasks 

Este projeto é um sistema de gerenciamento de tarefas.

## Funcionalidades

1. **Autenticação de Usuários:**    
    - [x] Cadastro de usuários via seeder.
    - [x] Login e logout seguros utilizando token JWT.

2. **CRUD de Tarefas:**
    - [x] **Criar:** Adicionar novas tarefas ao sistema.
    - [x] **Ler:** Visualizar todas as tarefas disponíveis.
    - [x] **Atualizar:** Editar informações das tarefas existentes.
    - [x] **Deletar:** Remover tarefas do sistema.

3. **Filtro de Tarefas:**
    - [x] Filtrar tarefas por título.    

4. **Atribuição de Tarefas a Usuários:** `(TODO)`
    - [ ] Associar tarefas a usuários específicos.
    - [ ] Permitir que cada usuário visualize apenas as tarefas atribuídas a ele.

## Tecnologias Utilizadas

#### Backend
- Docker e docker-compose
- Laravel 8 com PHP 8.0,
- Postgresql 13.4
- Servidor Nginx
- Scribe para documentação da API

#### Frontend
- HTML5, CSS3 e JavaScript
- Fetch para requisições http
- Storage

## Instalação e Uso

1. Clone o repositório:
   ```
   git clone https://gitlab.com/thiagoalmeida.negocios/case-join-tasks.git
   ````
2. Demais Instruções descritas no arquivo `README.md`contido em cada uma das pastas `(backend e frontend)`.